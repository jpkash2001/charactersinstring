public class Counter {
    public static int count(String input) {
        int count = 0;
        for(int i = 0; i < input.length(); i++) {
            if(input.charAt(i) >= 'a' && input.charAt(i) <= 'z' || input.charAt(i) >= 'A' && input.charAt(i) <= 'Z')
                count++;
        }
        return count;
    }
}

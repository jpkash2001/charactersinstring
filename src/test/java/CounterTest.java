import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CounterTest {

    @Test
    public void shouldReturnThreeWhenTheInputStringIsDog() {
        String input = "Dog";
        int expectedNumberOfCharacters = 3;
        int actualNumberOfCharacters = Counter.count(input);
        assertEquals(expectedNumberOfCharacters, actualNumberOfCharacters);
    }

    @Test
    public void shouldReturnThirteenWhenTheInputIsPeakyWhiteSpaceBlinders() {
        String input = "Peaky Blinders";
        int expectedNumberOfCharacters = 13;
        int actualNumberOfCharacters = Counter.count(input);
        assertEquals(expectedNumberOfCharacters, actualNumberOfCharacters);
    }

    @Test
    public void shouldReturnFifteenWhenTheInputIsJohnDoe123AtGmailDotCom() {
        String input = "johndoe123@gmail.com";
        int expectedNumberOfCharacters = 15;
        int actualNumberOfCharacters = Counter.count(input);
        assertEquals(expectedNumberOfCharacters, actualNumberOfCharacters);
    }
}